# go gitlab-ci

## pipeline

`.gitlab-ci.yml` состоит из этапов `stage` в каждом из котором содержит задачи из `jobs`

[Пример](https://gitlab.com/el_mail/go-gitlab-ci/-/pipelines/329596169)

## stage

**build:** сборка приложения;

**testing:** автотесты;

**staging:** развёртывание приложения для Разработчиков, DevOps, Тестировщиков;

**pre-production:** развёртывание в «pre-production» для команды Тестировщиков;

**approve:** этап в котором Релиз-Инженер может принять или отказать в релиз на production;

**production:** релиз на production.

## jobs

**build**:

 - **build:** выполняет сборку приложения, в данном случае выполняется сборка docker image и публикует готовый образ в GitLab Container Registry

**testing**:
- **test unit:** выполняет все unit тесты приложения
- **test integration:** выполняет полное интеграционное тестирование в изолированной среде с зависимыми сервисами
- **test selenium:** выполняет визуальное тестирование в изолированной среде где есть selenium
- **test local/cloud loadtest:** выполняет нагрузочное тестирование в изолированной среде или например в среде где можно пробросить доступ к cloud решению k6s

**staging**:

 - **deploy to dev-(1,2,3):** это набор сред для Разработчиков
 - **deploy to devops-(1,2,3):** это набор сред для DevOps-инженеров
 - **deploy to qa-(1,2,3):** это набор сред для Тестировщиков

**pre-production**:

 - **deploy to preprod:** это среда идентичная c production (иногда перебрасывается "живой" трафик)

**approve**:

 - **approve:** включает возможность релиза на production
 - **NOT approve:** выключает возможность релиза на production

**production**:

 - **deploy to PRODUCTION:** Релиз на production возможен только после успешного развёртывания на `pre-production` и выполнения задания `approve`

## Как это используется?

При каждом `git push` в ветки с префиксом `feat/` и `infra/` запускает этапы: `build, testing, staging`


Пример использования:
- `git branch feat/notification` - ветка с которой работает Разработчик
- `git branch infra/rabbitmq` - ветка с которой работает DevOps или SRE инженер


теги с префиксом `v` или `rc` запускает этапы: `build, testing, staging, pre-production, approve, production`

Пример использования:
- `git tag v1.0.1` - релиз версия
- `git tag rc-07.07.2021` - релиз кандидат
