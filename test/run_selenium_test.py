from pyvirtualdisplay import Display
from selenium import webdriver

display = Display(visible=0, size=(800, 600))
display.start()

browser = webdriver.Firefox()
browser.get('http://0.0.0.0:8080/ping')
print(browser.page_source)

browser.quit()
display.stop()