FROM python:3

WORKDIR /usr/src/app

RUN pip install --no-cache-dir requests pytest

COPY run_integration_test.py run_integration_test.py

CMD [ "pytest", "-v", "run_integration_test.py" ]
