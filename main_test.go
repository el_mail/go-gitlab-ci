package main

import (
	"testing"
)

func TestPong(t *testing.T) {
	message := Pong()
	if message != "pong" {
		t.Errorf(`Pong() = %s; want "pong"`, message)
	}
}
