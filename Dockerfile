# This file is a template, and might need editing before it works on your project.
FROM golang:1.15-alpine AS builder

RUN apk --update --no-cache add gcc g++ make ca-certificates git tzdata

WORKDIR /usr/src/app

COPY go.mod go.sum ./
RUN go mod download

COPY . .
RUN go build -o app && go test -c -o app.test

FROM alpine:3.5

# We'll likely need to add SSL root certificates
RUN apk --no-cache add ca-certificates

WORKDIR /usr/local/bin
COPY --from=builder /usr/src/app/app /usr/src/app/app.test ./
